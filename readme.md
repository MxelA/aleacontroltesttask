# Laravel Test App 5.8

## Requirements

- PHP 7.2+
- [Composer](https://getcomposer.org/download)
- [Bower](https://bower.io/)
- [Npm](https://docs.npmjs.com/cli/install)


## Setup notes


- Copy .env.example to .env
- Setup DB credentials in .env file
- Run: composer install
- Run: bower install
- Run: npm install
- Run: npm run dev
- Run: php artisan migrate
- Run: php artisan db:seed
- Run: php artisan passport:install
- Run: php artisan serve

## Note Setup Cron
- Run: crontab -e
- Paste: * * * * * cd <location-of-project> && php artisan schedule:run >> /dev/null 2>&1
- Save document

> If you not setup cron for running game, then you will start manual game with command:

- php artisan lucky6:start-round
