<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLucky6GameRoundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lucky6_game_rounds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('betting_location_id');
            $table->unsignedInteger('start_game_at');
            $table->boolean('finish')->default(false);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('betting_location_id')
                ->references('id')
                ->on('betting_locations');

            $table->index(['betting_location_id', 'start_game_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lucky6_game_rounds');
    }
}
