<?php

use Illuminate\Database\Seeder;

class DataBettingLocationSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\BettingLocation::class)->create([
            'admin_user_id' => \App\Models\AdminUser::role(\App\Models\Role::ROLE_ADMIN_LOCATION)->first()->id
        ]);
    }
}
