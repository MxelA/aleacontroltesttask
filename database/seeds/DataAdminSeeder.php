<?php

use Illuminate\Database\Seeder;

class DataAdminSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = factory(\App\Models\AdminUser::class)->create([
            'name' => 'admin',
            'email' => 'admin@lucky6.com',
            'password' => bcrypt('admin'), // password
        ]);
        $admin->assignRole(\App\Models\Role::ROLE_ADMIN);

        $adminBettingLocation = factory(\App\Models\AdminUser::class)->create([
            'name' => 'Admin Location',
            'email' => 'admin.location@lucky6.com',
            'password' => bcrypt('admin'), // password
        ]);
        $adminBettingLocation->assignRole(\App\Models\Role::ROLE_ADMIN_LOCATION);

    }
}
