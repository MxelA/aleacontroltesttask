<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DataRoleSeeder::class);
        $this->call(DataAdminSeeder::class);
        $this->call(DataUserSeeder::class);
        $this->call(DataBettingLocationSeeder::class);
    }
}
