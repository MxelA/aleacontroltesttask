<?php

use Illuminate\Database\Seeder;

class DataRoleSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Role::create(['name' => \App\Models\Role::ROLE_ADMIN, 'guard_name'=>'admin']);
        \App\Models\Role::create(['name' => \App\Models\Role::ROLE_ADMIN_LOCATION, 'guard_name'=>'admin']);
    }
}
