<?php

use Illuminate\Database\Seeder;

class DataUserSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\User::class)->create([
            'name' => 'player',
            'email' => 'player@lucky6.com',
            'password' => bcrypt('admin'), // password
            'admin_user_id' => \App\Models\AdminUser::role(\App\Models\Role::ROLE_ADMIN_LOCATION)->first()->id
        ]);
    }
}
