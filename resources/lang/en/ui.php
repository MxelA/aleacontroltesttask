<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'navigation' => 'Navigation',
    'online'     => 'Online',
    'welcome_to' => 'Welcome to',
    'save'       => 'Save',
    'new'        => 'New',
    'filter'     => 'Filter',
    'filters'    => 'Filters',
    'select...'  => 'Select...',
    'search'     => 'Search',
    'sign-out'   => 'Sign Out',
    'login'      => 'Login',
    'actions'    => [
        'singular' => 'Action',
        'plural'   => 'Actions'
    ],
    'lucky6' => [
        'tickets'   => [
            'singular'  => 'Lucky6 Ticket',
            'plural'    => 'Lucky6 Tickets',
            'create'    => 'Create Lucky6 Ticket',
            'statistic' => 'Lucky6 Statistic',
            'fields' => [
                'id'         => 'ID',
                'round'      => 'Round',
                'created_at' => 'Created at',
                'date-from'  => 'Date From',
                'date-to'    => 'Date To',
                'total-hits' => 'Total Hits',
            ]
        ]
    ],

];
