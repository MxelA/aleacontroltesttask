<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'betting-location' => [
        'singular' => 'Betting Location',
        'plural'   => 'Betting Locations',
        'fields'   => [
            'name'  => 'Name',
            'admin' => 'Admin'
        ],
        'messages' => [
            'create-success' => 'Betting Location successfully created!',
            'update-success' => 'Betting Location successfully updated!',
            'delete-success' => 'Betting Location successfully deleted!',
        ]
    ],
    'user' => [
        'singular'  => 'User',
        'plural'    => 'Users',
        'location-admins' => 'Location Admins',
        'location-admin' => 'Location Admin',
        'fields' => [
            'name'      => 'Name',
            'email'     => 'Email',
            'password'     => 'Password'
        ],
        'messages' => [
            'success-create-user' => 'User successfully created',
            'success-update-user' => 'User successfully updated',
            'success-delete-user' => 'User successfully deleted',
        ]
    ],
    'players' => [
        'singular'  => 'Player',
        'plural'    => 'Players'
    ]

];
