

<div class="box box-success box-solid">
    <div class="box-header with-border">
        <h4 class="box-title"><strong>{{ trans('ui.lucky6.tickets.singular') }} {{ $lucky6Ticket->id? '"' . $lucky6Ticket->name .'"': '' }}</strong></h4>
    </div>
    {!! Form::model($lucky6Ticket, ['route' => $formRoute, 'method' => $formMethod]) !!}
    <div class="box-body" id="lucky6-ticket-form">
        <div v-for="i in 48">
            <div class="col-xs-1 margin-bottom">
                <div :class="{ 'square': true, 'square-selected':(checkIfExistNumber(i) > -1)  }" @click="selectNumber(i)">
                    <div class="square-content">
                        @{{ i }}
                    </div>
                </div>
            </div>
        </div>
        <div v-if="selectedNumbers.length">
            <input type="hidden" v-for="(number,i) in selectedNumbers" :name="'numbers['+i+']'" :value="number">
        </div>
    </div>
    <div class="box-footer">
        <button type="submit" class="btn btn-primary">{{ trans('ui.save') }}</button>
    </div>
    {!! Form::close() !!}
</div>


@section('js')
    <script>
        const vueAppLucky6TicketForm = new Vue({
            el: '#lucky6-ticket-form',
            data: {
                selectedNumbers: [],
            },
            methods: {
                selectNumber: function (number) {
                    if(number) {
                        let index = this.checkIfExistNumber(number)
                        if(index >= 0) {
                            this.selectedNumbers.splice(index,1);
                        } else if (this.selectedNumbers.length < 6) {
                            this.selectedNumbers.push(number);
                        }
                    }
                },
                checkIfExistNumber: function (number) {

                    if(number) {
                        return _.indexOf(this.selectedNumbers, number);
                    }

                    return -1;
                }
            },
            mounted: function () {
                    console.log('load');
            }
        });
    </script>
@append
