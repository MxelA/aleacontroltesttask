@extends('layouts.dashboard.dashboard_template')

@section('content-header-title')
    {{ trans('ui.create') }}
@stop

@section('additionalInputs')

@stop

@section('content')
    @include('player.lucky6.form', ['formRoute' => 'player.lucky6.ticket.store', 'formMethod' => 'POST'])
@stop
