@extends('layouts.dashboard.dashboard_template')

@section('content')
    <div style="margin-top: 20%; color:darkgrey">
        <strong><h2 class="text-center">{{ trans('ui.welcome_to') }} CUSTOMER PANEL</h2></strong>
        <strong><h2 class="text-center">{{ env('APP_NAME') }}</h2></strong>
    </div>
@stop