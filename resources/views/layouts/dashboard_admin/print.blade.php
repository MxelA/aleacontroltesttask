<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="UTF-8">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- Theme style -->
    {{--<link href="{{ mix("/css/adminlte.css")}}" rel="stylesheet" type="text/css" />--}}
    {{--<link href="{{ mix("/css/dashboard.css")}}" rel="stylesheet" type="text/css" />--}}
    @yield('head')
</head>
<body class="sidebar-mini skin-blue">
    @yield('content')

    @yield('js')

    </body>


</html>