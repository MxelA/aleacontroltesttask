<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
    <meta charset="UTF-8">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Material Icon -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="{{ mix("/css/adminlte.css")}}" rel="stylesheet" type="text/css" />
    <link href="{{ mix("/css/dashboard.css")}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body class="sidebar-mini skin-blue">
    <div class="wrapper">

        <!-- Header -->
        @include('layouts.dashboard_admin.header')

        <!-- Sidebar -->
        @include('layouts.dashboard_admin.sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" id="content">

            <div class="col-xs-12" style="margin-top: 10px;">
                @section('messages')
                    @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Success</h4>
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    @if(Session::has('warning'))
                        <div class="alert alert-warning alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Warning!</h4>
                            {{ Session::get('warning') }}
                        </div>
                    @endif

                    @if(Session::has('error') || count($errors) > 0)
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            @if(Session::has('error'))
                                {{ Session::get('error') }}
                            @endif

                            @if(count($errors) > 0)
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    @endif
                @show
            </div>


            <!-- Main content -->
            <section class="content">
                @include('partials.dashboard.filters')
                <div class="row">
                    <div class="col-xs-12">
                        @yield('content')
                    </div>
                </div>
            </section>
        </div>

        <!-- Footer -->
        @include('layouts.dashboard_admin.footer')

    </div>

    <!-- AdminLTE Theme JS -->
    <script src="{{ mix("/js/adminlte.js") }}" type="text/javascript"></script>

    <!-- App JS -->
    <script src="{{ mix("/js/dashboard_app.js") }}" type="text/javascript"></script>

    @yield('js')

    </body>
</html>