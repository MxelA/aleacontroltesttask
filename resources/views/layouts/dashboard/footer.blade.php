<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        <!-- mozda logo -->
    </div>
    <!-- Default to the left -->
    <strong>Copyright © {{ date("Y") }} <a href="/">Smart Business Management</a>.</strong> All rights reserved.
</footer>