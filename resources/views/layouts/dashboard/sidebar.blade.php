<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <section class="sidebar" style="height: auto;">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ asset("/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image">
                </div>

            <div class="pull-left info">
                <p>User</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('ui.online') }}</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('ui.search') }}">
                <span class="input-group-btn">
                  <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu tree" data-widget="tree">
            <li class="header text-uppercase">{{ trans('ui.navigation') }}</li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-ticket"></i>
                    <span>{{ trans('ui.lucky6.tickets.singular') }}</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">
                    <li class="">
                        <a href="{{ route('player.lucky6.ticket.create') }}">
                            <i class="fa fa-ticket"></i> <span>{{ trans('ui.lucky6.tickets.create') }}</span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{ route('player.lucky6.ticket.index') }}">
                            <i class="fa fa-ticket"></i> <span>{{ trans('ui.lucky6.tickets.plural') }}</span>
                        </a>
                    </li>
                </ul>
            </li>

        </ul>
    </section>
</aside>