@extends('layouts.dashboard_admin.dashboard_template')

@section('content')
    <div style="margin-top: 20%; color:darkgrey">
        <strong><h2 class="text-center">{{ trans('ui.welcome_to') }} ADMIN PANEL</h2></strong>
        <strong><h2 class="text-center">{{ env('APP_NAME') }}</h2></strong>
    </div>
@stop