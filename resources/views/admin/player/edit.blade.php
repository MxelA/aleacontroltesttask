@extends('layouts.dashboard_admin.dashboard_template')

@section('content-header-title')
    {{ trans('ui.update') }}
@stop

@section('additionalInputs')

@stop

@section('content')
    @include('admin.player.form', ['formRoute' => ['admin.player.update', $user->id], 'formMethod' => 'PUT'])
@stop
