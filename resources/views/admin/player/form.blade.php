

<div class="box box-success box-solid">
    <div class="box-header with-border">
        <h4 class="box-title"><strong>{{ trans('ui_admin.players.singular') }} {{ $user->id? '"' . $user->name .'"': '' }}</strong></h4>
    </div>
    {!! Form::model($user, ['route' => $formRoute, 'method' => $formMethod]) !!}
    <div class="box-body">
        <div class="row">
            <div class="col-lg-7 col-xs-12">
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('name', trans('ui_admin.user.fields.name')) !!}
                        {!! Form::text('name', $user->name, ['class' => 'form-control', 'placeholder' => trans('ui_admin.user.fields.name')]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('email', trans('ui_admin.user.fields.email')) !!}
                        {!! Form::text('email', $user->email, ['class' => 'form-control', 'placeholder' => trans('ui_admin.user.fields.email')]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('password', trans('ui_admin.user.fields.password')) !!}
                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => trans('ui_admin.user.fields.password')]) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <button type="submit" class="btn btn-primary">{{ trans('ui.save') }}</button>
    </div>
    {!! Form::close() !!}
</div>


@section('js')
@append
