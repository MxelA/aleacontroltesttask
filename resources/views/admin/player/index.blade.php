@extends('layouts.dashboard_admin.dashboard_template')
<?php
    $AdminLTETableSorter = \AdminLTE\Laravel\Html\AdminLTETableSorter::create([
        'headings' => [
            ['name' => 'name', 'title' => trans('ui_admin.user.fields.name')],
            ['email' => 'name', 'title' => trans('ui_admin.user.fields.name')],
            ['title' => trans('ui.actions.singular'), 'sort' => false],
        ],
        'paginator' => $players,
        'filters'   => true,
    ]);
?>
@section('filters')
    {{--<div class="col-xs-12 col-lg-2">--}}
        {{--<div class="form-group">--}}
            {{--{!! Form::label('project_id', trans('ui.tasks.fields.project')) !!}--}}
            {{--{!! Form::select('project_id', $projects->pluck('name', 'id'), request('project_id'), ['class' => 'form-control','placeholder' => trans('ui.select...')]); !!}--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection

@section('content')

    <div class="row" id="user-index-page">
        <div class="col-xs-12">
            <div class="box box-success box-solid">

                <div class="box-header">
                    <h3 class="box-title">
                        <strong>
                            {{ trans('ui_admin.players.plural') }} ({{ $players->total() }})
                        </strong>
                    </h3>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row margin-bottom">
                        <div class="col-xs-12">
                            <a href="{{ route('admin.player.create') }}" class="btn btn-primary">
                                <i class="fa fa-plus"></i>
                                {{ trans('ui.new') }}
                            </a>
                        </div>
                    </div>

                    <div class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover dataTable table-condensed" role="grid">
                                        <thead>
                                        {!! $AdminLTETableSorter->table() !!}
                                        </thead>
                                        <tbody>
                                        @foreach($players as $user)
                                            <tr>
                                                <td>{{ $user->name }}</td>
                                                <td>{{ $user->email }}</td>
                                                <td>
                                                    <a class="btn btn-info" href="{{route('admin.player.edit', $user->id)}}">
                                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                                    </a>

                                                    <button type="button" class="btn btn-info"  @click="deleteAction({{$user->id}})">
                                                        {!! Form::model($user, ['route' => ['admin.player.destroy', $user->id], 'method' => 'DELETE', 'ref'=>"delete-" . $user->id]) !!}
                                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                        {!! Form::close() !!}
                                                    </button>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="list-inline text-center">{!! $players->appends(request()->all()) !!}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="v-cloak-loading" v-cloak>
            <modal :show-modal="deleteModal.showModal">
                <h3 slot="header">Delete User</h3>
                <p slot="body">Do you want to delete User?</p>
                <button slot="footer" class="btn btn-default" @click="deleteModalOk">Ok</button>
                <button slot="footer" class="btn btn-default" @click="deleteModalCancel">Cancel</button>
            </modal>
        </div>
    </div>
@stop

@section('js')
    <script>
        const vueApp = new Vue({
            el: '#user-index-page',
            data: {
                deleteModal: {
                    showModal: false,
                    id: null,
                },
            },
            methods: {
                deleteAction: function (taskId) {
                    this.deleteModal.id = taskId;
                    this.deleteModal.showModal = true;
                },
                deleteModalCancel:function () {
                    this.deleteTaskModal.showModal = false;
                },
                deleteModalOk:function () {
                    const form = this.$refs['delete-' + this.deleteModal.id];
                    form.submit();
                }
            },
            mounted: function () {
                $('#modal').on('hidden.bs.modal', () => {
                    this.deleteModal.showModal = false;
                });
            }
        });
    </script>
@endsection
