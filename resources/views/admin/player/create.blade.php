@extends('layouts.dashboard_admin.dashboard_template')

@section('content-header-title')
    {{ trans('ui.create') }}
@stop

@section('additionalInputs')

@stop

@section('content')
    @include('admin.player.form', ['formRoute' => 'admin.player.store', 'formMethod' => 'POST'])
@stop
