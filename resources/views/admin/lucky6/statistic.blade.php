@extends('layouts.dashboard_admin.dashboard_template')

@section('filters')

    <div class="col-xs-12 col-lg-2">
        <div class="form-group">
            {!! Form::label('date_from', trans('ui.lucky6.tickets.fields.date-from')) !!}
            {!! Form::text('date_from', request('date_from'), ['class' => 'form-control','placeholder' => trans('ui.select...'), 'id' => 'date-from-filter']); !!}
        </div>
    </div>
    <div class="col-xs-12 col-lg-2">
        <div class="form-group">
            {!! Form::label('date_to', trans('ui.lucky6.tickets.fields.date-to')) !!}
            {!! Form::text('date_to', request('date_to'), ['class' => 'form-control','placeholder' => trans('ui.select...'), 'id' => 'date-to-filter']); !!}
        </div>
    </div>
@endsection

@section('content')

    <div class="row" id="lucky6-statistic-page">
        <div class="col-xs-12">
            <div class="box box-success box-solid">

                <div class="box-header">
                    <h3 class="box-title">
                        <strong>
                            {{ trans('ui.lucky6.tickets.statistic') }}
                        </strong>
                    </h3>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <div class="chart-container" style="position: relative; height:80vh;">
                        <canvas id="myChart"></canvas>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@stop

@section('js')
    <script src="{{ mix("/js/Chart.js") }}" type="text/javascript"></script>

    <script>
        const vueApp = new Vue({
            el: '#lucky6-statistic-page',
            data: {
                data: {!! $ticketsStatistic->count() ? json_encode($ticketsStatistic->toArray()): json_encode([]) !!},
            },
            computed: {
                label: function () {
                    return Object.keys(this.data)
                },
                dataSet: function () {
                    let payment = {
                        label: '# of Payment',
                        data: [],
                        fillColor: 'rgba(255, 99, 132, 0.2)',
                        backgroundColor:'rgba(255, 99, 132, 0.2)',
                        borderColor: 'rgba(255, 99, 132, 0.2)',
                        borderWidth: 1
                    } ;

                    let payoff = {
                        label: '# of Payoff',
                        data: [],
                        backgroundColor: [
                            'rgba(54, 162, 235, 0.2)',
                        ],
                        borderColor: [
                            'rgba(54, 162, 235, 1)',
                        ],
                        borderWidth: 1
                    }

                    Object.keys(this.data).map(key => {
                        if(this.data[key].length == 1) {
                            payment.data.push(parseInt(this.data[key][0].payment));
                            payoff.data.push(parseInt(this.data[key][0].payoff))
                        } else {
                            let sum = this.data[key].reduce(function (total, value) {
                                total.payment = parseInt(total.payment) + parseInt(value.payment);
                                total.payoff  = parseInt(total.payoff) + parseInt(value.payoff);

                                return total
                            });

                            payment.data.push(parseInt(sum.payment));
                            payoff.data.push(parseInt(sum.payoff))
                        }
                    });

                    return [payment, payoff]
                }
            },
            methods: {
            },
            mounted: function () {
                var ctx = document.getElementById("myChart").getContext("2d");
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: this.label,
                        datasets: this.dataSet
                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,

                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });
            }
        });
    </script>



@endsection
