@extends('layouts.dashboard_admin.dashboard_template')
<?php
    $AdminLTETableSorter = \AdminLTE\Laravel\Html\AdminLTETableSorter::create([
        'headings' => [
            ['name' => 'round_id', 'title' => trans('ui.lucky6.tickets.fields.round')],
            ['name' => 'created_at', 'title' => trans('ui.lucky6.tickets.fields.created_at')],
            ['name' => 'total_hits', 'title' => trans('ui.lucky6.tickets.fields.total-hits')],
            ['title' => trans('ui.actions.singular'), 'sort' => false],
        ],
        'paginator' => $lucky6Tickets,
        'filters'   => true,
    ]);
?>
@section('filters')
    <div class="col-xs-12 col-lg-2">
        <div class="form-group">
            {!! Form::label('date_from', trans('ui.lucky6.tickets.fields.date-from')) !!}
            {!! Form::text('date_from', request('date_from'), ['class' => 'form-control','placeholder' => trans('ui.select...'), 'id' => 'date-from-filter']); !!}
        </div>
    </div>
    <div class="col-xs-12 col-lg-2">
        <div class="form-group">
            {!! Form::label('date_to', trans('ui.lucky6.tickets.fields.date-to')) !!}
            {!! Form::text('date_to', request('date_to'), ['class' => 'form-control','placeholder' => trans('ui.select...'), 'id' => 'date-to-filter']); !!}
        </div>
    </div>
@endsection

@section('content')

    <div class="row" id="lucky6-index-page">
        <div class="col-xs-12">
            <div class="box box-success box-solid">

                <div class="box-header">
                    <h3 class="box-title">
                        <strong>
                            {{ trans('ui.lucky6.tickets.plural') }}
                        </strong>
                    </h3>
                </div>

                <!-- /.box-header -->
                <div class="box-body">

                    <div class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover dataTable table-condensed" role="grid">
                                        <thead>
                                        {!! $AdminLTETableSorter->table() !!}
                                        </thead>
                                        <tbody>
                                        @foreach($lucky6Tickets as $ticket)
                                            <tr>
                                                <td>{{ $ticket->lucky6_game_round_id }}</td>
                                                <td>{{ $ticket->created_at }}</td>
                                                <td>{{ $ticket->total_hits?? 'Not Play' }}</td>
                                                <td>
                                                    {{--<a class="btn btn-info" href="{{route('admin.player.edit', $user->id)}}">--}}
                                                        {{--<i class="fa fa-pencil" aria-hidden="true"></i>--}}
                                                    {{--</a>--}}

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="list-inline text-center">{!! (get_class($lucky6Tickets) == 'Illuminate\Support\Collection') ? '':  $lucky6Tickets->appends(request()->all())!!}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@stop

@section('js')
    <script src="{{ mix("/js/bootstrap-material-datetimepicker.js") }}" type="text/javascript"></script>

    <script>
        const vueApp = new Vue({
            el: '#lucky6-index-page',
            data: {
            },
            methods: {
            },
            mounted: function () {
                $('#date-to-filter').bootstrapMaterialDatePicker({time: false});
                $('#date-from-filter').bootstrapMaterialDatePicker({time: false}).on('change', function (e, date) {
                    $('#date-to-filter').bootstrapMaterialDatePicker('setMinDate', date);
                });
            }
        });
    </script>
@endsection
