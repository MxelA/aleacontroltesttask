@extends('layouts.dashboard_admin.dashboard_template')

@section('content-header-title')
    {{ trans('ui.create') }}
@stop

@section('additionalInputs')

@stop

@section('content')
    @include('admin.user.form', ['formRoute' => 'admin.user.store', 'formMethod' => 'POST'])
@stop
