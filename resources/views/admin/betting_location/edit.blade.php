@extends('layouts.dashboard_admin.dashboard_template')

@section('content-header-title')
    {{ trans('ui.Edit') }}
@stop

@section('additionalInputs')

@stop

@section('content')
    @include('admin.betting_location.form', ['formRoute' => ['admin.betting-location.update', $bettingLocation->id], 'formMethod' => 'PUT'])
@stop
