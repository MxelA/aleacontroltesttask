

<div class="box box-success box-solid">
    <div class="box-header with-border">
        <h4 class="box-title"><strong>{{ trans('ui_admin.user.location-admin') }} {{ $bettingLocation->id? '"' . $bettingLocation->name .'"': '' }}</strong></h4>
    </div>
    {!! Form::model($bettingLocation, ['route' => $formRoute, 'method' => $formMethod]) !!}
    <div class="box-body" id="betting-location-form">
        <div class="row">
            <div class="col-lg-7 col-xs-12">
                <div class="box-body">
                    <div class="form-group">
                        {!! Form::label('name', trans('ui_admin.betting-location.fields.name')) !!}
                        {!! Form::text('name', $bettingLocation->name, ['class' => 'form-control', 'placeholder' => trans('ui_admin.betting-location.fields.name')]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('admin', trans('ui_admin.betting-location.fields.admin')) !!}
                        <v-select
                                label="name"
                                :placeholder="'{{ trans('ui_admin.user.location-admin') }}'"
                                :options="adminLocationUsers"
                                :filterable="true"
                                :clearable="false"
                                v-model="adminLocationUser"
                                @search="onSearchAdminLocationUsers"
                        ></v-select>
                        <input type="hidden" name="admin_user_id" v-model="adminLocationUser.id">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <button type="submit" class="btn btn-primary">{{ trans('ui.save') }}</button>
    </div>
    {!! Form::close() !!}
</div>

@section('js')
    <script>
        const vueAppBettingLocation = new Vue({
            el: '#betting-location-form',
            data: {
                adminLocationUsers:[],
                adminLocationUser:{!! $bettingLocation->adminLocation? json_encode($bettingLocation->adminLocation) : "{}" !!}
            },
            methods: {
                onSearchAdminLocationUsers: function (search, loading) {
                    this.fetchAdminLocationUsers(loading, search, this);
                },
                fetchAdminLocationUsers: _.debounce((loading, search, vm) => {
                    if(search) {
                        loading(true);
                        vm.$http.get('{{ route('api.admin.admin-location.index') }}' + '?search='+ search).then( response => {
                            vm.adminLocationUsers = response.data.data;
                            loading(false);
                        });
                    }

                }, 350),
            },
            mounted: function () {
            }
        });
    </script>
@append
