@extends('layouts.dashboard_admin.dashboard_template')
<?php
    $AdminLTETableSorter = \AdminLTE\Laravel\Html\AdminLTETableSorter::create([
        'headings' => [
            ['name' => 'name', 'title' => trans('ui_admin.betting-location.fields.name')],
            ['title' => trans('ui.actions.singular'), 'sort' => false],
        ],
        'paginator' => $bettingLocations,
        'filters'   => true,
    ]);
?>
@section('filters')
    {{--<div class="col-xs-12 col-lg-2">--}}
        {{--<div class="form-group">--}}
            {{--{!! Form::label('project_id', trans('ui.tasks.fields.project')) !!}--}}
            {{--{!! Form::select('project_id', $projects->pluck('name', 'id'), request('project_id'), ['class' => 'form-control','placeholder' => trans('ui.select...')]); !!}--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection

@section('content')

    <div class="row" id="user-index-page">
        <div class="col-xs-12">
            <div class="box box-success box-solid">

                <div class="box-header">
                    <h3 class="box-title">
                        <strong>
                            {{ trans('ui_admin.betting-location.plural') }} ({{ $bettingLocations->total() }})
                        </strong>
                    </h3>
                </div>

                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row margin-bottom">
                        <div class="col-xs-12">
                            <a href="{{ route('admin.betting-location.create') }}" class="btn btn-primary">
                                <i class="fa fa-plus"></i>
                                {{ trans('ui.new') }}
                            </a>
                        </div>
                    </div>

                    <div class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover dataTable table-condensed" role="grid">
                                        <thead>
                                        {!! $AdminLTETableSorter->table() !!}
                                        </thead>
                                        <tbody>
                                        @foreach($bettingLocations as $location)
                                            <tr>
                                                <td>{{ $location->name }}</td>
                                                <td>
                                                    <a class="btn btn-info" href="{{route('admin.betting-location.edit', $location->id)}}">
                                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                                    </a>

                                                    <button type="button" class="btn btn-info"  @click="deleteTask({{$location->id}})">
                                                        {!! Form::model($location, ['route' => ['admin.betting-location.destroy', $location->id], 'method' => 'DELETE', 'ref'=>"delete-task-" . $location->id]) !!}
                                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                        {!! Form::close() !!}
                                                    </button>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="list-inline text-center">{!! $bettingLocations->appends(request()->all()) !!}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>

        <div class="v-cloak-loading" v-cloak>
            <modal :show-modal="deleteTaskModal.showModal">
                <h3 slot="header">Delete Task</h3>
                <p slot="body">Do you want to delete task?</p>
                <button slot="footer" class="btn btn-default" @click="deleteTaskModalOk">Ok</button>
                <button slot="footer" class="btn btn-default" @click="deleteTaskModalCancel">Cancel</button>
            </modal>
        </div>
    </div>
@stop

@section('js')
    <script>
        const vueApp = new Vue({
            el: '#content',
            data: {
                deleteTaskModal: {
                    showModal: false,
                    id: null,
                },
            },
            methods: {
                deleteTask: function (taskId) {
                    this.deleteTaskModal.id = taskId;
                    this.deleteTaskModal.showModal = true;
                },
                deleteTaskModalCancel:function () {
                    this.deleteTaskModal.showModal = false;
                },
                deleteTaskModalOk:function () {
                    const form = this.$refs['delete-task-' + this.deleteTaskModal.id];
                    form.submit();
                }
            },
            mounted: function () {
                $('#modal').on('hidden.bs.modal', () => {
                    this.deleteTaskModal.showModal = false;
                });
            }
        });
    </script>
@endsection
