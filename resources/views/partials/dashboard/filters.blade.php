@if(isset($AdminLTETableSorter) && $AdminLTETableSorter->filters )
    {!! Form::open(['method' => 'GET', 'url' => route(request()->route()->getName(), request()->all())]) !!}
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning box-solid {{ empty(request()->except(['sortedBy'])) ? 'collapsed-box' : '' }}">
                    <div class="box-header with-border">
                        <h4 class="box-title">
                            <strong>
                                {{ trans('ui.filters') }}
                            </strong>
                        </h4>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                                <i class="fa {{ empty(request()->all()) ? 'fa-minus' : 'fa-plus'}}" ></i>
                            </button>
                        </div>
                    </div>

                    <div class="box-body"  {!! empty(request()->all()) ? 'style="display: none;"' : '' !!} >
                        {{--<div class="col-xs-12 col-lg-2">--}}
                            {{--<div class="form-group">--}}
                                {{--{!! Form::label('search', trans('ui.search')) !!}--}}
                                {{--{!! Form::text('search',  request()->get('search'), ['class' => 'form-control', 'placeholder' => trans('ui.search')]) !!}--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        @yield('filters')

                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-default pull-right">
                            <i class="fa fa-search"></i>
                            {{ trans('ui.filter') }}
                        </button>
                    </div>

                </div>
            </div>
        </div>

    {!! Form::close() !!}
@endif
