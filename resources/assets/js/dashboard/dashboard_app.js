
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./dashboard_bootstrap');

window.moment       = require('moment');
window.swal         = require('sweetalert');
window.Compact      = require('vue-color/src/components/Compact');

import Modal from '../components/dashboard/modal.vue';
import ProductAttribute from '../components/dashboard/productAttribute.vue';
import CreateAdditionalFields from '../components/dashboard/createAdditionalFields.vue';
import AdditionalField from '../components/dashboard/additionalField.vue';
import UploadTemplate from '../components/dashboard/uploadTemplate.vue';

import vSelect from 'vue-select'
import "vue-select/src/scss/vue-select.scss";
import {round} from './vue_filters';

Vue.component('modal', Modal);
Vue.component('product-attribute', ProductAttribute);
Vue.component('v-select', vSelect);
Vue.component('create-additional-fields', CreateAdditionalFields);
Vue.component('additional-field', AdditionalField);
Vue.component('upload-template', UploadTemplate);


Vue.filter('parseDate', function (date, dateFormat) {
    return moment(date).format(dateFormat);
});

Vue.filter('round', function (value, decimals) {
    return round(value, decimals);
});