const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/dashboard/dashboard_app.js', 'public/js')
    .js('resources/assets/bower/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .scripts([
        'resources/assets/bower/jquery/jquery.js',
        'resources/assets/bower/typeahead.js/dist/typeahead.bundle.js',
        'resources/assets/bower/bootstrap/dist/js/bootstrap.js',
        'resources/assets/bower/select2/dist/js/select2.js',
        'resources/assets/bower/admin-lte/dist/js/adminlte.js'

    ], 'public/js/adminlte.js')
    .combine([
        'node_modules/chart.js/dist/Chart.css',
        'resources/assets/bower/bootstrap/dist/css/bootstrap.css',
        'resources/assets/bower/admin-lte/dist/css/AdminLTE.min.css',
        'resources/assets/bower/admin-lte/dist/css/skins/skin-blue.min.css',
        'resources/assets/css/dashboard/select2.css',
        'resources/assets/bower/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css',
        'Modules/CalendarEvent/Resources/assets/css/fullcalendar.min.css'
    ],  'public/css/adminlte.css')
    .copy([
        'node_modules/chart.js/dist/Chart.js',
    ], 'public/js/Chart.js')
    .copy([
        'resources/assets/bower/typeahead.js/dist/bloodhound.js',
    ], 'public/js/bloodhound.js')
    .copy([
        'resources/assets/css/login.css'
    ], 'public/css')
    .copy([
        'resources/assets/css/dashboard/dashboard.css'
    ], 'public/css')
    .copy([
        'resources/assets/bower/admin-lte/dist/img/user2-160x160.jpg',
        'resources/assets/image/login_bg.jpg'
    ], 'public/img')
    .copy([
        'resources/assets/bower/bootstrap/fonts',
        'resources/assets/bower/bootstrap-material-datetimepicker/font'
    ], 'public/fonts');


if(mix.inProduction()){
    mix.version();
}
