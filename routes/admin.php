<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'admin','middleware' => ['web', 'auth:admin']], function() {
    Route::get('/', function () {
        return view('admin.welcome');
    });

    Route::resource('betting-location', 'Admin\BettingLocationController', ['as' => 'admin']);
    Route::resource('user', 'Admin\UserController', ['as' => 'admin'] );
    Route::resource('player', 'Admin\PlayerController', ['as' => 'admin'] );
    Route::resource('lucky6/ticket', 'Admin\Lucky6TicketController', ['except' => ['destroy', 'edit', 'update'],'as' => 'admin.lucky6']);
    Route::get('lucky6/statistic', ['uses' => 'Admin\Lucky6TicketController@statistic','as' => 'admin.lucky6.statistic']);
});
