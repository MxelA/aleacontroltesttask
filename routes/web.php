<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::group(['prefix' => 'player', 'middleware' => ['web', 'auth:web']], function() {

    Route::get('/', function () {
        return view('player.welcome');
    });

    Route::resource('lucky6/ticket', 'Player\Lucky6TicketController', ['as' => 'player.lucky6']);
});

