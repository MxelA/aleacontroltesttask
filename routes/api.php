<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['prefix' => 'admin','middleware' => ['api', 'auth:apiAdmin']], function() {
    Route::get('admin-location', ['as' => 'api.admin.admin-location.index', 'uses' => 'Admin\Api\ApiAdminLocationController@index']);
});
