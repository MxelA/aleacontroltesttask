<?php

namespace AdminLTE\Laravel\Html;

use Collective\Html\FormFacade;
use Illuminate\Support\Facades\Request;

/**
 * Class FormBuilder.
 */
class AdminLTETableSorter extends FormFacade
{

    //default sort_by variable in url (sort_by)
    private $sort_by_variable;
    //default sort_type variable in url (sort_type)
    private $sort_type_variable;
    private $sort_type_default;

    //classes which plugin uses for links
    private $order_active_class;
    private $order_asc_class;
    private $order_desc_class;
    private $order_next_asc_class;
    private $order_next_desc_class;
    public  $filters;

    private $headings;
    private $paginator;
    private $config;

    private $sort_by;
    private $sort_type;
    private $template;
    private $templateDisabled;

    public static function create($data)
    {
        $tableSorter = new self();

        //load from app config
        $templateConfig         = '<th class="%s" rowspan="1" colspan="1"><a href="%s"> %s</a><span class="%s" style="float: right"></span></th>';
        $templateDisabledConfig = '<th class="%s"> %s </th>';

        $tableSorter->sort_by_variable      = 'orderBy';
        $tableSorter->sort_type_variable    = 'sortedBy';
        $tableSorter->sort_type_default     = 'ASC';
        $tableSorter->order_active_class    = 'fa fa-sort';
        $tableSorter->order_asc_class       = 'fa fa-sort-amount-asc';
        $tableSorter->order_desc_class      = 'fa fa-sort-amount-desc';
        $tableSorter->order_next_asc_class  = 'fa fa-sort-amount-asc';
        $tableSorter->order_next_desc_class = 'fa fa-sort-amount-desc';

        $tableSorter->headings  = $data['headings'];
        $tableSorter->paginator = $data['paginator'];
        $tableSorter->filters   = (isset($data['filters'])) ? $data['filters']: false;

        $config = (isset($data['config'])) ? $data['config'] : [];

        //load overridden config
        $tableSorter->template          = (isset($config['template'])) ? $config['template'] : $templateConfig;
        $tableSorter->templateDisabled  = (isset($config['templateDisabled'])) ? $config['templateDisabled'] : $templateDisabledConfig;
        $tableSorter->sort_by           = (isset($config['sort_by'])) ? $config['sort_by'] : Request::get($tableSorter->sort_by_variable);
        $tableSorter->sort_type         = (isset($config['sort_type'])) ? $config['sort_type'] : Request::get($tableSorter->sort_type_variable);

        return $tableSorter;
    }

    public function table()
    {
        $string = '';
        foreach ($this->headings as $heading) {
            $name   = (isset($heading['name']))   ? $heading['name']  : '';
            $title  = (isset($heading['title']))  ? $heading['title'] : '';
            $sort   = (isset($heading['sort']))   ? $heading['sort']  : true;
            $class  = (isset($heading['class']))  ? $heading['class'] . ' '  : '';
            $thClass = (isset($heading['th_class']))  ? $heading['th_class']: '';
            if ($this->sort_by != $name) {
                $sort_type_this = 'ASC';
            } else {
                if ($this->sort_type == 'ASC') {
                    $sort_type_this = 'DESC';
                } else {
                    $sort_type_this = 'ASC';
                }
            }

            if ($this->sort_by == $name) {
                if ($sort_type_this == 'ASC') {
                    $class .= $this->order_desc_class.' ';
                } else {
                    $class .= $this->order_asc_class.' ';
                }
            } else {
                $class .= $this->order_active_class;

            }

            if ($sort == true) {
                $paginator_tmp = clone $this->paginator;
                $string .= sprintf(
                    $this->template,
                    $thClass,
                    $paginator_tmp->appends([$this->sort_by_variable => $name, $this->sort_type_variable => $sort_type_this])->url($paginator_tmp->currentPage()),
                    $title,
                    $class
                );
            } else {
                $string .= sprintf($this->templateDisabled, $thClass. 'order-disabled ', $title);
            }

        }

        return $string;
    }
}
