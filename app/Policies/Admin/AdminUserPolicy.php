<?php

namespace App\Policies\Admin;

use App\Models\AdminUser;
use App\Models\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminUserPolicy
{
    use HandlesAuthorization;



    public function userManage($authUser, AdminUser $user = null)
    {
        return $authUser->hasRole(Role::ROLE_ADMIN);

    }
}
