<?php

namespace App\Policies\Admin;

use App\Models\AdminUser;
use App\Models\Role;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\User;

class UserPolicy
{
    use HandlesAuthorization;


    public function playerManage($authUser, User $user = null)
    {
        $isAdminLocationRole = $authUser->hasRole(Role::ROLE_ADMIN_LOCATION);
        if(!$user) {
            return $isAdminLocationRole;
        }

        return $isAdminLocationRole && $user->admin_user_id === $authUser->id;
    }
}
