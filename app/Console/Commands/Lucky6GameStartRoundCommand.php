<?php

namespace App\Console\Commands;

use App\Models\BettingLocation;
use App\Models\Lucky6GameRound;
use App\Models\Lucky6Ticket;
use App\Services\Lucky6GameService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Pagination\LengthAwarePaginator;

class Lucky6GameStartRoundCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lucky6:start-round';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->query = BettingLocation::where('active', true);

        $this->bettingLocationPaginateIterator($this->query->paginate());
    }

    private function bettingLocationPaginateIterator(LengthAwarePaginator $bettingLocations)
    {
        foreach ($bettingLocations->items() as $bettingLocation) {
            $this->startRound($bettingLocation);
        }

        if($bettingLocations->hasMorePages()) {
            $currentPage    = $bettingLocations->currentPage();
            $nextPage       = ++$currentPage;

            $this->createLucky6GameIteration($this->query->paginate(2, ['*'], 'page', $page = $nextPage));
        }

    }

    private function startRound(BettingLocation $bettingLocation)
    {
        $rounds = Lucky6GameService::startLucky6Round($bettingLocation);

        foreach ($rounds as $round)
        {
            $roundDate = Carbon::parse($round->start_game_at)->startOfDay();

            $tables = Lucky6GameService::getLucky6TicketsTable($roundDate->timestamp, $roundDate->timestamp, $bettingLocation);

            $ticketsQuery = null;

            if(count($tables)){
                $tickets = (new Lucky6Ticket([], $tables[0]->table_name))
                    ->where('lucky6_game_round_id', $round->id)
                    ->whereNull('total_hits')
                    ->get()
                ;

                foreach ($tickets as $ticket) {
                    $ticket->update(['total_hits' => random_int(0,6)]);
                }

                $round->finish = 1;
                $round->save();
            }
        }
    }
}
