<?php

namespace App\Http\Controllers\Player;

use App\Http\Requests\Admin\BettingLocation\CreateBettingLocationRequest;
use App\Http\Requests\Player\Lucky6\CreateLucky6TicketRequest;
use App\Models\BettingLocation;
use App\Models\Lucky6Ticket;
use App\Services\Lucky6GameService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;

class Lucky6TicketController extends Controller
{
    public function index(Request $request)
    {


        $bettingLocation    = BettingLocation
            ::criteriaByAdminLocation(\Auth::user()->admin_user_id)
            ->first()
        ;

        $tables         = Lucky6GameService::getLucky6TicketsTable($request->input('date_from'), $request->input('date_to'), $bettingLocation);
        $ticketsQuery   = null;

        for ($i = 0; $i < count($tables); $i++) {
            if($i === 0) {
                $ticketsQuery = (new Lucky6Ticket([], $tables[$i]->table_name))
                    ->criteriaByPlayer(\Auth::user())
                ;
                continue;
            }

            $ticketsQuery = $ticketsQuery->union(
                (new Lucky6Ticket([], $tables[$i]->table_name))
                ->criteriaByPlayer(\Auth::user())
            );
        }

        if(!$ticketsQuery) {
            $lucky6Tickets = new Paginator([], 15);
        } else {
            $lucky6Tickets = $ticketsQuery
                ->orderBy('id', 'DESC')
                ->paginate()
            ;
        }


        return view('player.lucky6.index', compact('lucky6Tickets'));
    }

    public function create()
    {
        $bettingLocation    = BettingLocation
            ::criteriaByAdminLocation(\Auth::user()->admin_user_id)
            ->first()
        ;

        $tableName = Lucky6GameService::getLucky6TicketCurrentTable($bettingLocation);

        $lucky6Ticket = new Lucky6Ticket([], $tableName);

        return view('player.lucky6.create', compact('lucky6Ticket'));
    }

    public function store(CreateLucky6TicketRequest $request)
    {
        $bettingLocation    = BettingLocation
            ::criteriaByAdminLocation(\Auth::user()->admin_user_id)
            ->first()
        ;
        $tableName = Lucky6GameService::getLucky6TicketCurrentTable($bettingLocation);

        $lucky6Ticket = new Lucky6Ticket([], $tableName);


        $lucky6Ticket->fill($request->all())
        ;
        $lucky6Ticket->save();
        return redirect()->route('player.lucky6.ticket.index')->with(['success' => trans('ui_admin.betting-location.messages.create-success')]);
    }

    public function edit($bettingLocationId)
    {
        $bettingLocation = BettingLocation::findOrFail($bettingLocationId);

        return view('admin.betting_location.edit', compact('bettingLocation'));
    }

    public function update(CreateBettingLocationRequest $request, $bettingLocationId)
    {
        $bettingLocation = BettingLocation::findOrFail($bettingLocationId);
        $bettingLocation->update($request->all());

        return redirect()->route('admin.betting-location.index')->with(['success' => trans('ui_admin.betting-location.messages.update-success')]);
    }

    public function destroy($bettingLocationId)
    {
        $bettingLocation = BettingLocation::findOrFail($bettingLocationId);
        $bettingLocation->delete();

        return back()->with(['success' => trans('ui_admin.betting-location.messages.delete-success')]);
    }
}
