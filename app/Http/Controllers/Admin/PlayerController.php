<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Player\CreatePlayerRequest;
use App\Http\Requests\Admin\User\CreateUserRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class PlayerController extends Controller
{

    /**
     * List of Players
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->authorize('playerManage', User::class);

        $players = User::criteriaByAdminUser(\Auth::user())
            ->paginate()
        ;

        return view('admin.player.index', compact('players'));
    }


    /**
     * Create Player
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('playerManage', User::class);

        $user = new User();

        return view('admin.player.create', compact('user'));
    }


    /**
     * Store player in DB
     * @param CreatePlayerRequest $request
     * @return $this
     */
    public function store(CreatePlayerRequest $request)
    {
        $this->authorize('playerManage', User::class);
        $user = new User();

        $user->admin_user_id = \Auth::user()->id;

        $user->fill($request->all())
            ->save()
        ;

        return redirect()->route('admin.player.index')->with(['success' => trans('ui_admin.user.messages.success-create-user')]);
    }

    /**
     * Edit Player
     * @param $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($userId)
    {
        $user = User::findOrFail($userId);

        $this->authorize('playerManage', $user);

        return view('admin.player.edit', compact('user'));
    }


    /**
     * Update Player
     * @param CreatePlayerRequest $request
     * @param $userId
     * @return $this
     */
    public function update(CreatePlayerRequest $request, $userId)
    {
        $user = User::findOrFail($userId);

        $this->authorize('playerManage', $user);

        $user->update($request->all());

        return redirect()->route('admin.player.index')->with(['success' => trans('ui_admin.user.messages.success-update-user')]);
    }


    /**
     * Destroy Player
     * @param $userId
     * @return $this
     */
    public function destroy($userId)
    {
        $user = User::findOrFail($userId);

        $this->authorize('playerManage', $user);

        $user->delete();

        return back()->with(['success' => trans('ui_admin.user.messages.success-delete-user')]);
    }
}
