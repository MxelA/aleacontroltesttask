<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\User\CreateUserRequest;
use App\Http\Requests\CreateTaskRequest;
use App\Models\AdminUser;
use App\Models\BettingLocation;
use App\Models\Project;
use App\Models\Role;
use App\Models\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    /**
     * List of Admin Users
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->authorize('userManage', AdminUser::class);

        $adminLocationUsers = AdminUser::role(Role::ROLE_ADMIN_LOCATION)
            ->paginate()
        ;

        return view('admin.user.index', compact('adminLocationUsers'));
    }


    /**
     * Create Admin location user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('userManage', AdminUser::class);

        $user = new AdminUser();

        return view('admin.user.create', compact('user'));
    }


    /**
     * Save admin location user
     * @param CreateUserRequest $request
     * @return $this
     */
    public function store(CreateUserRequest $request)
    {
        $this->authorize('userManage', AdminUser::class);

        $admin = AdminUser::create($request->all());
        $admin->assignRole(Role::ROLE_ADMIN_LOCATION);

        return redirect()->route('admin.user.index')->with(['success' => trans('ui_admin.user.messages.success-create-user')]);
    }

    /**
     * Edit Admin Location user
     * @param $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($userId)
    {
        $user = AdminUser::findOrFail($userId);

        $this->authorize('userManage', $user);

        return view('admin.user.edit', compact('user'));
    }


    /**
     * Update Admin location user
     * @param CreateUserRequest $request
     * @param $userId
     * @return $this
     */
    public function update(CreateUserRequest $request, $userId)
    {
        $user = AdminUser::findOrFail($userId);

        $this->authorize('userManage', $user);

        $user->update($request->all());

        return redirect()->route('admin.user.index')->with(['success' => trans('ui_admin.user.messages.success-update-user')]);
    }

    /**
     * Delete admin location user
     * @param $userId
     * @return $this
     */
    public function destroy($userId)
    {
        $user = AdminUser::findOrFail($userId);

        $this->authorize('userManage', $user);

        $user->delete();

        return back()->with(['success' => trans('ui_admin.user.messages.success-delete-user')]);
    }
}
