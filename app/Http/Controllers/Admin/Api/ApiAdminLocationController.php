<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Resources\Admin\AdminLocationResource;
use App\Models\AdminUser;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class ApiAdminLocationController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('userManage', AdminUser::class);

        $adminLocationUsers = AdminUser::role(Role::ROLE_ADMIN_LOCATION)
            ->criteriaByName($request->input('search'))
        ;

        return AdminLocationResource::collection($adminLocationUsers->paginate());
    }
}
