<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\BettingLocation\CreateBettingLocationRequest;
use App\Models\BettingLocation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BettingLocationController extends Controller
{
    public function index(Request $request)
    {
        $bettingLocations = BettingLocation::paginate();

        return view('admin.betting_location.index', compact('bettingLocations'));
    }

    public function create()
    {
        $bettingLocation = new BettingLocation();

        return view('admin.betting_location.create', compact('bettingLocation'));
    }

    public function store(CreateBettingLocationRequest $request)
    {
        BettingLocation::create($request->all());

        return redirect()->route('admin.betting-location.index')->with(['success' => trans('ui_admin.betting-location.messages.create-success')]);
    }

    public function edit($bettingLocationId)
    {
        $bettingLocation = BettingLocation::findOrFail($bettingLocationId);

        return view('admin.betting_location.edit', compact('bettingLocation'));
    }

    public function update(CreateBettingLocationRequest $request, $bettingLocationId)
    {
        $bettingLocation = BettingLocation::findOrFail($bettingLocationId);
        $bettingLocation->update($request->all());

        return redirect()->route('admin.betting-location.index')->with(['success' => trans('ui_admin.betting-location.messages.update-success')]);
    }

    public function destroy($bettingLocationId)
    {
        $bettingLocation = BettingLocation::findOrFail($bettingLocationId);
        $bettingLocation->delete();

        return back()->with(['success' => trans('ui_admin.betting-location.messages.delete-success')]);
    }
}
