<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\BettingLocation\CreateBettingLocationRequest;
use App\Http\Requests\Player\Lucky6\CreateLucky6TicketRequest;
use App\Models\BettingLocation;
use App\Models\Lucky6Ticket;
use App\Models\Role;
use App\Services\Lucky6GameService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;

class Lucky6TicketController extends Controller
{
    public function index(Request $request)
    {

        $tables = Lucky6GameService::getLucky6TicketsTable($request->input('date_from'), $request->input('date_to'), \Auth::user()->bettingLocation);
        $ticketsQuery = null;

        for ($i = 0; $i < count($tables); $i++) {
            if ($i === 0) {
                $ticketsQuery = (new Lucky6Ticket([], $tables[$i]->table_name));
                continue;
            }

            $ticketsQuery = $ticketsQuery->union(
                (new Lucky6Ticket([], $tables[$i]->table_name))
            );
        }

        if (!$ticketsQuery) {
            $lucky6Tickets = new Paginator([], 15);
        } else {
            $lucky6Tickets = $ticketsQuery
                ->orderBy('lucky6_game_round_id', 'DESC')
                ->paginate();
        }


        return view('admin.lucky6.index', compact('lucky6Tickets'));
    }

    public function statistic()
    {

        $tables = Lucky6GameService::getLucky6TicketsTable(Carbon::now()->addDays(-7)->startOfDay()->toDayDateTimeString(), null, \Auth::user()->bettingLocation);

        $ticketsStatisticQuery = null;
        for ($i = 0; $i < count($tables); $i++) {
            if ($i === 0) {
                $ticketsStatisticQuery = (new Lucky6Ticket([], $tables[$i]->table_name))
                    ->select(
                        \DB::raw('lucky6_game_rounds.created_at as created_at'),
                        \DB::raw('lucky6_game_rounds.start_game_at as start_game_at'),
                        \DB::raw('COUNT('.$tables[$i]->table_name.'.id) as payment'),
                        \DB::raw('SUM(IF ('.$tables[$i]->table_name.'.total_hits > 3,'.$tables[$i]->table_name.'.total_hits,0)) as payoff')
                    )
                    ->leftJoin('lucky6_game_rounds', $tables[$i]->table_name.'.lucky6_game_round_id', '=', 'lucky6_game_rounds.id')
                    ->groupBy(\DB::raw('HOUR(lucky6_game_rounds.created_at)'),$tables[$i]->table_name.'.id' )
                ;
                continue;
            }

            $ticketsStatisticQuery = $ticketsStatisticQuery->union(
                (new Lucky6Ticket([], $tables[$i]->table_name))
                    ->select(
                        \DB::raw('lucky6_game_rounds.created_at as created_at'),
                        \DB::raw('lucky6_game_rounds.start_game_at as start_game_at'),
                        \DB::raw('COUNT('.$tables[$i]->table_name.'.id) as payment'),
                        \DB::raw('SUM(IF ('.$tables[$i]->table_name.'.total_hits > 3,'.$tables[$i]->table_name.'.total_hits,0)) as payoff')
                    )
                    ->leftJoin('lucky6_game_rounds', $tables[$i]->table_name.'.lucky6_game_round_id', '=', 'lucky6_game_rounds.id')
                    ->groupBy(\DB::raw('HOUR(lucky6_game_rounds.created_at)'),$tables[$i]->table_name.'.id')
            );
        }

        if(!$ticketsStatisticQuery) {
            $ticketsStatistic = collect([]);
        } else {
            $ticketsStatistic = $ticketsStatisticQuery
                ->get()
            ;
        }

        $ticketsStatistic = $ticketsStatistic->groupBy(function($item){
            return $item->created_at->toDateTimeString();
        });

        return view('admin.lucky6.statistic', compact('ticketsStatistic'));
    }
}
