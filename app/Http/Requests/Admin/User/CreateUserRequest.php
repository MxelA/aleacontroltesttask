<?php

namespace App\Http\Requests\Admin\User;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected $userId;
    public function rules()
    {
        $this->userId = $this->route('user');

        $this->sanitize();

        if($this->userId) {
            return [
                'name'          => 'required|string|max:255',
                'password'      => 'nullable|string|min:6',
                'email'         => 'required|string|email|max:255|unique:admin_users,email,' . $this->userId,
            ];
        }

        return [
            'name'          => 'required|string|max:255',
            'email'         => 'required|string|email|max:255|unique:admin_users',
            'password'      => 'required|string|min:6'
        ];
    }

    private function sanitize() {
        $input = $this->all();

        if($input['password']) {
            $input['password'] = bcrypt($input['password']);
        } else {
            unset($input['password']);
        }

        $this->replace($input);
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
