<?php

namespace App\Http\Requests\Admin\BettingLocation;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class CreateBettingLocationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $bettingLocationId = $this->route('betting_location');

        if($bettingLocationId) {
            return [
                'name'          => 'required|string|max:255',
                'admin_user_id' => 'required|unique:betting_locations,admin_user_id,'. $bettingLocationId
            ];
        }

        return [
            'name'          => 'required|string|max:255',
            'admin_user_id' => 'required|unique:betting_locations,admin_user_id'
        ];

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
