<?php

namespace App\Http\Requests\Player\Lucky6;

use App\Services\Lucky6GameService;
use Illuminate\Foundation\Http\FormRequest;

class CreateLucky6TicketRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        $this->sanitize();

        return [
            'numbers' => 'required|array|min:6|max:6',
            'numbers.*' => 'required|integer|min:1|max:48'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    private function sanitize()
    {
        $input = $this->all();

        $input['player_id']             = \Auth::user()->id;
        $input['lucky6_game_round_id']  = Lucky6GameService::getLucky6GameRoundForBetting(\Auth::user()->betting_location)->id;

        $this->replace($input);
    }
}
