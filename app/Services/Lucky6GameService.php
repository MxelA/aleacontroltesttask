<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 4/18/20
 * Time: 1:13 PM
 */
namespace  App\Services;

use App\Models\BettingLocation;
use App\Models\Lucky6GameRound;
use App\Models\Lucky6Ticket;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Lucky6GameService
{

    const TABLE_BASE_NAME = 'lucky6_tickets';

    public static function getLucky6TicketCurrentTable(BettingLocation $bettingLocation = null)
    {
        $timeStampTableName   = Carbon::now()->startOfDay()->timestamp;

        return self::TABLE_BASE_NAME .'_'. $timeStampTableName. ($bettingLocation ? '_'.$bettingLocation->id: '');
    }

    public static function getLucky6TicketsTable($from=null, $to=null, BettingLocation $bettingLocation = null)
    {
        $timeStampFrom = $from? Carbon::parse($from)->startOfDay(): Carbon::now()->startOfDay();
        $timeStampTo   = $to ?  Carbon::parse($to)->startOfDay(): Carbon::now()->startOfDay();

        if($timeStampFrom) {
            $tableNames = [];
            $period     = CarbonPeriod::between($timeStampFrom, $timeStampTo);

            foreach ($period as $date) {
                $tableNames[] = "'".Lucky6Ticket::TABLE_BASE_NAME . '_' . $date->timestamp . ($bettingLocation? '_'.$bettingLocation->id:'')."%'";
            }

            $queryString = implode(' OR table_name LIKE ', $tableNames);

            return \DB::select(\DB::raw('
              SELECT table_name FROM information_schema.tables
              WHERE 
                table_schema = ? AND 
                `table_name` LIKE '.$queryString.' 
            ;
            '),[env('DB_DATABASE')]);
        }
    }

    /**
     * Create Lucky6 ticket table
     * @param $tableName
     */
    public static function createLucky6TTicketTable($tableName)
    {
        if(!Schema::connection('mysql')->hasTable($tableName))
        {
            Schema::connection('mysql')->create($tableName, function(Blueprint $table) use ($tableName)
            {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('player_id');
                $table->unsignedBigInteger('lucky6_game_round_id');
                $table->json('numbers');
                $table->smallInteger('total_hits')->nullable();
                $table->timestamps();
                $table->softDeletes();

                $table->foreign('player_id')
                    ->references('id')
                    ->on('users');

                $table->foreign('lucky6_game_round_id')
                    ->references('id')
                    ->on('lucky6_game_rounds');
            });
        }

    }

    public static function startLucky6Round(BettingLocation $bettingLocation)
    {
        $currentTime                    = Carbon::now()->seconds(00);
        $diffInMinutesForNextRound      = ($currentTime->minute % Lucky6GameRound::ITERATION_GAME_EVERY);

        return Lucky6GameRound::where('start_game_at', '<=', $currentTime->addMinutes(($diffInMinutesForNextRound? $diffInMinutesForNextRound * (-1) : 0))->timestamp)
            ->criteriaByBettingLocation($bettingLocation)
            ->where('finish', false)
            ->get()
        ;

    }

    public static function getLucky6GameRoundForBetting(BettingLocation $bettingLocation): Lucky6GameRound
    {

        $currentTime                    = Carbon::now()->seconds(00);
        $diffInMinutesForNextRound      = Lucky6GameRound::ITERATION_GAME_EVERY - ($currentTime->minute % Lucky6GameRound::ITERATION_GAME_EVERY);
        $nexIterationTime               = Carbon::parse($currentTime->timestamp)->addMinutes($diffInMinutesForNextRound);

        $lastLucky6GameRound = Lucky6GameRound::criteriaByStartTime($nexIterationTime->timestamp)
            ->criteriaByBettingLocation()
            ->criteriaByFinish(false)
            ->first()
        ;

        if(!$lastLucky6GameRound) {
            $lastLucky6GameRound = Lucky6GameRound::create([
                'betting_location_id'   => $bettingLocation->id,
                'start_game_at'         => $nexIterationTime->timestamp,
                'created_at'            => $nexIterationTime,
                'updated_at'            => $nexIterationTime
            ]);

        }

        return $lastLucky6GameRound;
    }
}