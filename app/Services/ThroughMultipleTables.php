<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 4/18/20
 * Time: 1:13 PM
 */
namespace  App\Services;

use Carbon\Carbon;

trait ThroughMultipleTables
{
    protected $generateTableInInterval = 'day';
    protected $baseNameTable;
    protected $timeStampTableName;

    public function generateCurrentTableName(string $baseNameTable, $id = null)
    {
        $this->baseNameTable        = $baseNameTable;
        $this->timeStampTableName   = $this->getDateTimeTableName()->timestamp;

        $this->table = $baseNameTable .'_'. $this->timeStampTableName. ($id ? '_'.$id: '');
    }

    private function getDateTimeTableName(Carbon $time = null): Carbon
    {
        switch ($this->generateTableInInterval) {
            case 'month':
                return $time? $time->startOfMonth(): Carbon::now()->startOfMonth();
            default:
                return $time? $time->startOfDay(): Carbon::now()->startOfDay();
        }
    }

//    public function scopeCriteriaPaginateThroughMultipleTablesFromTo($query, $from = null, $to = null, $id = null)
//    {
//        $timeStampFrom = $from? $this->getDateTimeTableName(Carbon::parse($from)): null;
//        $timeStampTo   = $to ?  $this->getDateTimeTableName(Carbon::parse($to)): $this->getDateTimeTableName();
//
//        if($from) {
//            $tableNames = [];
//            $period     = CarbonPeriod::between($timeStampFrom, $timeStampTo);
//
//            foreach ($period as $date) {
//                $tableNames[] = "'".$this->baseNameTable . '_' . $date->timestamp . ($id? '_'.$id:'') . "%'";
//            }
//
//            $queryString = implode(' OR table_name LIKE ', $tableNames);
//
//            $tables = \DB::select(\DB::raw('
//              SELECT table_name FROM information_schema.tables
//              WHERE
//                table_schema = ? AND
//                `table_name` LIKE '.$queryString.'
//            ;
//            '),[env('DB_DATABASE')]);
//
//            if(count($tables))
//            {
////                dd($query->getModel()->setTable($tables[0]->table_name));
//                $model = $query->getModel();
//                $model->setTable($tables[0]->table_name);
//                $query->setModel($model);
////                $query->getModel()->setTable($tables[0]->table_name);
////                $query = new static($this->attributes, $tables[0]->table_name);
//
//                for($i = 1; $i < count($tables); $i++) {
//                    $clone = clone $query;
//                    $cloneModel = $clone->getModel();
//                    $cloneModel->setTable($tables[$i]->table_name);
//                    $clone->setModel($cloneModel);
//                    $query = $query->union($cloneModel);
//                }
//            }
//        }
//
//        return $query;
//
//    }

}