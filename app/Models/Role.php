<?php

namespace App\Models;


class Role extends \Spatie\Permission\Models\Role
{

    // Main Roles constant and Must implement
    const ROLE_ADMIN                = 'admin';
    const ROLE_ADMIN_LOCATION       = 'admin-location';


}
