<?php

namespace App\Models;

use App\Services\Lucky6GameService;
use App\Services\ThroughMultipleTables;
use Illuminate\Database\Eloquent\Model;

class Lucky6Ticket extends Model
{
    use ThroughMultipleTables;

    protected $fillable = [
        'numbers',
        'player_id',
        'lucky6_game_round_id',
        'total_hits'
    ];

    protected $casts = [
        'numbers' => 'json',
    ];

    const TABLE_BASE_NAME= 'lucky6_tickets';

    public function __construct(array $attributes = [], $tableName = null)
    {
        parent::__construct($attributes);

        if($tableName) {
            $this->table = $tableName;
            Lucky6GameService::createLucky6TTicketTable($this->table);
        }

    }


    /*******************************
     *          SCOPES
     ******************************/
    public function scopeCriteriaByPlayer($query, User $player)
    {
        if($player) {
            return $query->where('player_id', $player->id);
        }
    }

    public function scopeCriteriaByPlayedTicket($query)
    {
        return $query->whereNotNull('total_hits');
    }
}
