<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BettingLocation extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'admin_user_id'
    ];

    public function adminLocation()
    {
        return $this->belongsTo(AdminUser::class, 'admin_user_id');
    }

    public function scopeCriteriaByAdminLocation($query, $adminUserId = null)
    {
        if($adminUserId) {
            return $query->where('admin_user_id', $adminUserId);
        }
    }
}
