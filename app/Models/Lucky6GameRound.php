<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lucky6GameRound extends Model
{
    const ITERATION_GAME_EVERY              = 5;
    const CREATE_NEW_ROUND_BEFORE_ITERATION = 2;

    protected $fillable = [
        'betting_location_id',
        'start_game_at',
        'created_at',
        'updated_at'
    ];


    public function scopeCriteriaByStartTime($query, $timeStamp = null)
    {
        if($timeStamp) {
            return $query->where('start_game_at', $timeStamp);
        }
    }

    public function scopeCriteriaByBettingLocation($query, BettingLocation $bettingLocation = null)
    {
        if($bettingLocation) {
            return $query->where('betting_location_id', $bettingLocation->id);
        }
    }

    public function scopeCriteriaByFinish($query, bool $finish = null)
    {
        if($finish) {
            return $query->where('finish', $finish);
        }
    }
}
